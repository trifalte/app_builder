# This template will setup
# -default home route + controller
# -git + bitbucket private repo
# -push up to heroku (given that ssh keys are setup)

require 'debugger'

#an app_template converted from app_builder because rails 4 no longer uses the builder option
def modify_test
  gem 'rspec-rails', group: [:test, :development]
  run 'bundle install'
  generate 'rspec:install'
end

def modify_readme
  remove_file "README.rdoc"
  create_file "README.md", "TODO"
end

def setup_home
  generate :controller, "home index"
  route "root to: 'home\#index'"
  remove_file "public/index.html"
end

def setup_git
  user = ask("Which user for bitbucket?")
  git :init

  append_file ".gitignore", "#{IO.read("#{File.expand_path(File.dirname(__FILE__))}/templates/gitignore")}"
  run "cp config/database.yml config/example_database.yml"
  git add: ".", commit: "-m 'initial commit'"

  `curl --user '#{user}' https://api.bitbucket.org/1.0/repositories/ --data name='#{@app_name}' --data is_private='true' --data language='ruby'`
  git remote: "add origin ssh://git@bitbucket.org/#{user}/#{@app_name}.git"
  git push: "origin master"
end

def setup_heroku
  `heroku create #{@app_name}`
  git push: 'heroku master'
end

modify_test
modify_readme
setup_home
setup_git
setup_heroku